let mix = require("laravel-mix");
let path = require("path");

mix.setResourceRoot("../");
mix.setPublicPath(path.resolve("./"));

mix.webpackConfig({
  watchOptions: {
    ignored: [
      path.posix.resolve(__dirname, "./node_modules"),
      path.posix.resolve(__dirname, "./static/css"),
      path.posix.resolve(__dirname, "./static/js"),
    ],
  },
});

mix
  .js("static/js/app.js", "js")
  .postCss("static/css/app.css", "css")
  .postCss("static/css/editor-style.css", "css")
  .extract(["jquery"]); // Extract jQuery

if (mix.inProduction()) {
  mix.version();
} else {
  mix.options({ manifest: false });
}
